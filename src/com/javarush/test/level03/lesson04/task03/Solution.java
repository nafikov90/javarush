package com.javarush.test.level03.lesson04.task03;

/* StarCraft
Создать 10 зергов, 5 протосов и 12 терран.
Дать им всем уникальные имена.
*/

public class Solution
{
    public static void main(String[] args)
    {
        Zerg z1 = new Zerg();
        Zerg z2 = new Zerg();
        Zerg z3 = new Zerg();
        Zerg z4 = new Zerg();
        Zerg z5 = new Zerg();
        Zerg z6 = new Zerg();
        Zerg z7 = new Zerg();
        Zerg z8 = new Zerg();
        Zerg z9 = new Zerg();
        Zerg z10 = new Zerg();

        Protos p1 = new Protos();
        Protos p2 = new Protos();
        Protos p3 = new Protos();
        Protos p4 = new Protos();
        Protos p5 = new Protos();

        Terran t1 = new Terran();
        Terran t2 = new Terran();
        Terran t3 = new Terran();
        Terran t4 = new Terran();
        Terran t5 = new Terran();
        Terran t6 = new Terran();
        Terran t7 = new Terran();
        Terran t8 = new Terran();
        Terran t9 = new Terran();
        Terran t10 = new Terran();
        Terran t11 = new Terran();
        Terran t12 = new Terran();



    }

    public static class Zerg
    {
        public String name;
    }

    public static class Protos
    {
        public String name;
    }

    public static class Terran
    {
        public String name;
    }
}