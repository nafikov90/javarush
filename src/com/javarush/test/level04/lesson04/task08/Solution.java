package com.javarush.test.level04.lesson04.task08;

/* Треугольник
Ввести с клавиатуры три числа а, b, c – стороны предполагаемого треугольника.
Определить возможность существования треугольника по сторонам. Результат вывести на экран в следующем виде:
"Треугольник существует." - если треугольник с такими сторонами существует.
"Треугольник не существует." - если треугольник с такими сторонами не существует.
Подсказка: Треугольник существует только тогда, когда сумма любых двух его сторон больше третьей.
Требуется сравнить каждую сторону с суммой двух других.
Если хотя бы в одном случае сторона окажется больше суммы двух других, то треугольника с такими сторонами не существует.
*/

import java.io.*;

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String s = reader.readLine();
        String m = reader.readLine();
        String n = reader.readLine();
        int s1= Integer.parseInt(s);
        int m1 = Integer.parseInt(m);
        int n1 = Integer.parseInt(n);

        tr(s1, m1, n1);

            }
            public static void tr(int x, int y, int z) {

                if (x+y>z & x+z>y & z+y>x)
                    System.out.println("Треугольник существует.");
                else
                    System.out.println("Треугольник не существует.");
            }


}