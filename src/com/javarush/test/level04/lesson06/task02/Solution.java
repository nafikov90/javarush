package com.javarush.test.level04.lesson06.task02;

/* Максимум четырех чисел
Ввести с клавиатуры четыре числа, и вывести максимальное из них.
*/

import java.io.*;

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String a = reader.readLine();
        String b = reader.readLine();
        String c = reader.readLine();
        String d = reader.readLine();
        int x = Integer.parseInt(a);
        int y = Integer.parseInt(b);
        int z = Integer.parseInt(c);
        int m = Integer.parseInt(d);

        int o, p;
        o = maks(x, y);
        p = maks(z, m);
        System.out.println(maks(o, p));

        //напишите тут ваш код

    }
    public static int maks(int a, int b) {
        if (a>b)
            return a;
        else
            return b;
    }
}
