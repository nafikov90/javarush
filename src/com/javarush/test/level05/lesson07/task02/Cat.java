package com.javarush.test.level05.lesson07.task02;

/* Создать класс Cat
Создать класс Cat (кот) с пятью инициализаторами:
- Имя,
- Имя, вес, возраст
- Имя, возраст (вес стандартный)
- вес, цвет, (имя, адрес и возраст неизвестны, это бездомный кот)
- вес, цвет, адрес ( чужой домашний кот)
Задача инициализатора – сделать объект валидным. Например, если вес неизвестен, то нужно указать какой-нибудь средний вес. Кот не может ничего не весить. То же касательно возраста. А вот имени может и не быть (null). То же касается адреса: null.
*/

public class Cat
{
    public String name = null;
    public int ves;
    public int vozr;
    public String cvet = null;
    public String adress = null;


    public void initialize(String name) {
        this.name = name;
    }

    public void initialize(String name, int ves, int vozr) {
        this.name = name;
        this.ves = ves;
        this.vozr = vozr;
            }
    public void initialize(String name, int vozr) {
        this.name = name;
        this.ves = 4;
        this.vozr = vozr;
    }
    public void initialize(int ves, String cvet) {

        this.ves = ves;
        this.vozr = 5;
        this.cvet = cvet;
    }

    public void initialize(int ves, String cvet, String adress) {

        this.ves = ves;
        this.vozr = 5;
        this.cvet = cvet;
        this.adress = adress;
    }

            //напишите тут ваш код

}
