package com.javarush.test.level05.lesson07.task03;

/* Создать класс Dog
Создать класс Dog (собака) с тремя инициализаторами:
- Имя
- Имя, рост
- Имя, рост, цвет
*/

public class Dog
{
    public String name = null, cvet = null;
    public int rost;

    public void initialize(String a) {
        this.name = a;
    }
    public void initialize(String a, int b) {
        this.name = a;
        this.rost = b;
    }
    public void initialize(String a, int b, String cvet) {
        this.name = a;
        this.rost = b;
        this.cvet = cvet;
    }//напишите тут ваш код

}
