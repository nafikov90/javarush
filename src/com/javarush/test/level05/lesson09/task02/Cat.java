package com.javarush.test.level05.lesson09.task02;

/* Создать класс Cat
Создать класс Cat (кот) с пятью конструкторами:
- Имя,
- Имя, вес, возраст
- Имя, возраст (вес стандартный)
- вес, цвет, (имя, адрес и возраст – неизвестные. Кот - бездомный)
- вес, цвет, адрес ( чужой домашний кот)
Задача конструктора – сделать объект валидным. Например, если вес не известен, то нужно указать какой-нибудь средний вес. Кот не может ничего не весить. То же касательно возраста. А вот имени может и не быть (null). То же касается адреса: null.
*/

public class Cat
{
    public String name, color, adr;
    public int ves, age;

    public Cat(String name) {
        this.name = name;
    }
    public Cat(String name, int a, int v) {
        this.name = name;
        this.ves = v;
        this.age = a;

    }
    public Cat(String name, int a)
{
    this.name = name;
    this.age = a;
    this.ves = 3;
}
    public Cat(int a, String color)
    {
        this.age = 2;
        this.ves = a;
        this.color = color;

    }
    public Cat(int a, String color, String adr)
    {
        this.age = 2;
        this.ves = a;
        this.color = color;
        this.adr = adr;

    }

}
