package com.javarush.test.level07.lesson04.task01;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

/* Максимальное среди массива на 20 чисел
1. В методе initializeArray():
1.1. Создайте массив на 20 чисел
1.2. Считайте с консоли 20 чисел и заполните ими массив
2. Метод max(int[] array) должен находить максимальное число из элементов массива
*/

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        int[] bz = initializeArray();
        int max = max(bz);
        System.out.println(max);
    }
    public static int[] initializeArray() throws IOException {
        int[]bz = new int[20];
        Scanner lena = new Scanner(System.in);

        for (int i=0; i<20; i++){
            bz[i] = lena.nextInt();

            //Инициализируйте (создайте и заполните) массив тут
        }
        return bz;
    }

    public static int max(int[] bz) {
        int max = 0;
        for (int i=0; i<20; i++) {
            if (max<bz[i])
                max = bz[i];//Найдите максимальное значение в этом методе
        }
        return max;
    }
}
