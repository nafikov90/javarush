package com.javarush.test.level07.lesson04.task04;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;

/* Массив из чисел в обратном порядке
1. Создать массив на 10 чисел.
2. Ввести с клавиатуры 10 чисел и записать их в массив.
3. Расположить элементы массива в обратном порядке.
4. Вывести результат на экран, каждое значение выводить с новой строки.
*/

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        int[] a = new int[10];
        Scanner h = new Scanner(System.in);

        for (int i=0; i<10; i++) {
           a[i] = h.nextInt();
            //напишите тут ваш код
        }

        for (int j=0; j<a.length; j++) {
            int k = a.length - j - 1;
            System.out.println( a[k] );
        }




    }
}
