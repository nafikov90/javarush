package com.javarush.test.level07.lesson06.task01;

import java.util.ArrayList;
import java.util.Scanner;

/* 5 различных строчек в списке
1. Создай список строк.
2. Добавь в него 5 различных строчек.
3. Выведи его размер на экран.
4. Используя цикл выведи его содержимое на экран, каждое значение с новой строки.
*/
public class Solution
{
    public static void main(String[] args) throws Exception
    {
        ArrayList<String> salavat = new ArrayList<String>();


        for (int i=0; i<5; i++) {
            salavat.add(i, "Lopata");//напишите тут ваш код
        }
        System.out.println(salavat.size());

        for (int i=0; i<5; i++) {
            System.out.println(salavat.get(i));
        }

    }
}
