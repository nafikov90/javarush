package com.javarush.test.level07.lesson12.home06;

/* Семья
Создай класс Human с полями имя(String), пол(boolean),возраст(int), отец(Human), мать(Human). Создай объекты и заполни их так, чтобы получилось: Два дедушки, две бабушки, отец, мать, трое детей. Вывести объекты на экран.
Примечание:
Если написать свой метод String toString() в классе Human, то именно он будет использоваться при выводе объекта на экран.
Пример вывода:
Имя: Аня, пол: женский, возраст: 21, отец: Павел, мать: Катя
Имя: Катя, пол: женский, возраст: 55
Имя: Игорь, пол: мужской, возраст: 2, отец: Михаил, мать: Аня
…
*/


public class Solution
{
    public static void main(String[] args)
    {
        Human ch1 = new Human("Ch1", true, 62, null, null);
        Human ch2 = new Human("Ch2", false, 63, null, null);
        Human ch3 = new Human("Ch3", true, 64, null, null);
        Human ch4 = new Human("Ch4", false, 64, null, null);

        Human mother = new Human("M", false, 35, ch1, ch2);
        Human father = new Human("F", true, 35, ch3, ch4);

        Human GF1 = new Human("GF1", true, 5, father, mother);
        Human GM1 = new Human("GM1", false, 4, father, mother);

        System.out.println(ch1);
        System.out.println(ch2);//напишите тут ваш код
        System.out.println(ch3);//напишите тут ваш код
        System.out.println(ch4);
        System.out.println();//напишите тут ваш код
        System.out.println(father);//напишите тут ваш код
        System.out.println(mother);//напишите тут ваш код
        System.out.println();//напишите тут ваш код
        System.out.println(GF1);//напишите тут ваш код
        System.out.println(GM1);//напишите тут ваш код
    }

    public static class Human
    {
        String name;
        boolean sex;
        int age;
        Human father;
        Human mother;

        public Human(String name, boolean sex, int age, Human father, Human mother) {
            this.name = name;
            this.sex = sex;
            this.age = age;
            this.father = father;
            this.mother = mother;
        }

        /*public Human(String name, boolean sex, int age) {
            this.name = name;
            this.sex = sex;
            this.age = age;
        }*///напишите тут ваш код

        public String toString()
        {
            String text = "";
            text += "Имя: " + this.name;
            text += ", пол: " + (this.sex ? "мужской" : "женский");
            text += ", возраст: " + this.age;

            if (this.father != null)
                text += ", отец: " + this.father.name;

            if (this.mother != null)
                text += ", мать: " + this.mother.name;

            return text;
        }
    }

}
