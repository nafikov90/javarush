package com.javarush.test.level08.lesson08.task01;

import java.util.HashSet;
import java.util.Set;

/* 20 слов на букву «Л»
Создать множество строк (Set<String>), занести в него 20 слов на букву «Л».
*/

public class Solution
{
    public static HashSet<String> createSet()
    {
        Set<String> slovaL = new HashSet<String>();

        for(int i = 0; i < 20; i++)
            slovaL.add("Лимур " + i);
        return (HashSet<String>) slovaL;//напишите тут ваш код

    }
}
