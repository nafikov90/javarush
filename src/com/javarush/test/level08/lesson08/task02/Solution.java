package com.javarush.test.level08.lesson08.task02;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/* Удалить все числа больше 10
Создать множество чисел(Set<Integer>), занести туда 20 различных чисел.
Удалить из множества все числа больше 10.
*/

public class Solution
{
    public static HashSet<Integer> createSet()
    {
        Set<Integer> chisla = new HashSet<Integer>();
        for (int i=0; i<20; i++) {
            chisla.add(i);//напишите тут ваш код
        }

        return (HashSet<Integer>) chisla;



    }

    public static HashSet<Integer> removeAllNumbersMoreThan10(HashSet<Integer> set)
    {
        Iterator<Integer> it = set.iterator();

        while (it.hasNext()) {
            int s = it.next();
            if (s>10)
                it.remove();//напишите тут ваш код
        }

        return (HashSet<Integer>) set;

    }
}
