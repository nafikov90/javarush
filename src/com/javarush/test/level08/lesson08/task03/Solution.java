package com.javarush.test.level08.lesson08.task03;

import java.util.*;

/* Одинаковые имя и фамилия
Создать словарь (Map<String, String>) занести в него десять записей по принципу «Фамилия» - «Имя».
Проверить сколько людей имеют совпадающие с заданным имя или фамилию.
*/

public class Solution
{

    public static void main(String[] args)
    {
        System.out.println(getCountTheSameFirstName(createMap(), "Pirogov"));
        System.out.println(getCountTheSameLastName(createMap(), "Ivan"));
    }
    public static HashMap<String, String> createMap()
    {
        HashMap<String, String> nM = new HashMap<String, String>();
        nM.put("Ivan", "Pirogov");//напишите тут ваш код
        nM.put("Igor", "Pirogov");
        nM.put("AI", "Kymis");//напишите тут ваш код
        nM.put("ss", "marmon");//напишите тут ваш код
        nM.put("ad", "marmon");//напишите тут ваш код
        nM.put("sdd", "saas");//напишите тут ваш код
        nM.put("dfd", "dfdf");//напишите тут ваш код
        nM.put("dfdff", "asdfg");//напишите тут ваш код
        nM.put("axfd", "dcfd");//напишите тут ваш код
        nM.put("dfdfb", "fdgdfv");
        return nM;//напишите тут ваш код

    }

    public static int getCountTheSameFirstName(HashMap<String, String> map, String name)
    {
        int count = 0;//напишите тут ваш код
        for (String tmp : map.values()) {
            if (name.equals(tmp)) {
                count++;
            }
        }

        return count;

    }

    public static int getCountTheSameLastName(HashMap<String, String> map, String lastName)
    {
        int count = 0;
        Set<Map.Entry<String, String>> entrySet = map.entrySet();
        for (Map.Entry<String, String> pair : entrySet) {
            if (pair.getKey().equals(lastName)) {
                count++;//напишите тут ваш код
            }
        }
        return count;
    }
}
