package com.javarush.test.level08.lesson11.home01;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/* Set из котов
1. Внутри класса Solution создать public static класс кот – Cat.
2. Реализовать метод createCats, он должен создавать множество (Set) котов и добавлять в него 3 кота.
3. В методе main удалите одного кота из Set cats.
4. Реализовать метод printCats, он должен вывести на экран всех котов, которые остались во множестве. Каждый кот с новой строки.
*/

public class Solution
{
    public static void main(String[] args)
    {
        Set<Cat> cats = createCats();

        //System.out.println(cats);//напишите тут ваш код. пункт 3

        Iterator<Cat> i = cats.iterator();
        i.next();
        i.remove();
        printCats(cats);
    }

    public static class Cat {

    }

    public static Set<Cat> createCats()
    {
        Set<Cat> a = new HashSet<Cat>();
        a.add(new Cat());
        a.add(new Cat());
        a.add(new Cat());


        //напишите тут ваш код. пункт 2
        return a;
    }

    public static void printCats(Set<Cat> cats)
    {
        Iterator<Cat> i = cats.iterator();
        while (i.hasNext()) {

            System.out.println(i.next());// пункт 4
        }
    }

    // пункт 1
}
