package com.javarush.test.level08.lesson11.home04;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/* Минимальное из N чисел
1. Ввести с клавиатуры число N.
2. Считать N целых чисел и заполнить ими список - метод getIntegerList.
3. Найти минимальное число среди элементов списка - метод getMinimum.
*/

public class Solution
{
    public static void main(String[] args) throws Exception {
        List<Integer> integerList = getIntegerList();
        System.out.println(getMinimum(integerList));
    }

    public static int getMinimum(List<Integer> array) {
        int min = Collections.min(array);// Найти минимум
        return min;
    }

    public static List<Integer> getIntegerList() throws IOException {
        BufferedReader m = new BufferedReader(new InputStreamReader(System.in));
        String s = m.readLine();
        int a = Integer.parseInt(s);
        List<Integer> m5 = new ArrayList<>();

        for (int i=0; i<a; i++) {
            String h = m.readLine();
            m5.add(Integer.parseInt(h));
            //Тут создать и заполнить список
        }
        return m5;
    }
}
