package com.javarush.test.level08.lesson11.home06;

/* Вся семья в сборе
1. Создай класс Human с полями имя (String), пол (boolean), возраст (int), дети (ArrayList<Human>).
2. Создай объекты и заполни их так, чтобы получилось: два дедушки, две бабушки, отец, мать, трое детей.
3. Вывести все объекты Human на экран.
*/

import java.util.ArrayList;
import java.util.Arrays;

public class Solution
{
    public static void main(String[] args)
    {
        ArrayList<Human> list = new ArrayList<>();
        ArrayList<Human> list1 = new ArrayList<>();
        ArrayList<Human> list2 = new ArrayList<>();
        Human ch1 = new Human("Ch1", true, 2, list);
        Human ch2 = new Human("Ch2", true, 3, list);
        Human ch3 = new Human("Ch3", true, 4, list);

        list.add(ch1);
        list.add(ch2);
        list.add(ch3);

        Human mother = new Human("M", false, 35, list);
        Human father = new Human("F", true, 35, list);

        list1.add(mother);

        Human GF1 = new Human("GF1", true, 60, list1);
        Human GM1 = new Human("GM1", false, 60, list1);

        list2.add(father);

        Human GF2 = new Human("GF2", true, 62, list2);
        Human GM2 = new Human("GM2", false, 63, list2);

        Human[] family = {GF1, GF2, GM1, GM2, father, mother, ch1, ch2, ch3};

        for (Human h : family) {
            System.out.println(h);
        }
    }

    public static class Human
    {
        String name;
        boolean sex;
        int age;
        ArrayList<Human> children;

        public Human(String name, boolean sex, int age, ArrayList<Human> children) {
            this.name = name;
            this.sex = sex;
            this.age = age;
            this.children = children;
        }//напишите тут ваш код

        public String toString()
        {
            String text = "";
            text += "Имя: " + this.name;
            text += ", пол: " + (this.sex ? "мужской" : "женский");
            text += ", возраст: " + this.age;

            int childCount = this.children.size();
            if (childCount > 0)
            {
                text += ", дети: "+this.children.get(0).name;

                for (int i = 1; i < childCount; i++)
                {
                    Human child = this.children.get(i);
                    text += ", "+child.name;
                }
            }

            return text;
        }
    }

}
