package com.javarush.test.level08.lesson11.home06;

public class merge1
{
    public static void main(String[] args)
    {
        int[] arr = {5, 6, 2, 11, 8, 3, 1, 9};
    }

    public static void mergeSort(int[] arr, int low, int hi) {
        if (low == hi) {
            return;
        } else {
            int middle = low + hi / 2;
            mergeSort(arr, low, middle);
            mergeSort(arr, middle + 1, hi);

        }
    }
}
