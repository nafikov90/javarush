package com.javarush.test.level09.lesson11.home04;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/* Конвертер дат
Ввести с клавиатуры дату в формате «08/18/2013»
Вывести на экран эту дату в виде «AUG 18, 2013».
Воспользоваться объектом Date и SimpleDateFormat.
*/

public class Solution {

    public static void main(String[] args) throws Exception {
        SimpleDateFormat p1 = new SimpleDateFormat("MM/dd/yyyy");
        BufferedReader rr = new BufferedReader(new InputStreamReader(System.in));

        Date x = p1.parse(rr.readLine());
        //System.out.println(x);
        SimpleDateFormat p2 = new SimpleDateFormat("MMM dd, yyyy", Locale.US);

        System.out.println(p2.format(x).toUpperCase());



    }
}
