package com.javarush.test.level10.lesson11.home06;

/* Конструкторы класса Human
Напиши класс Human с 6 полями. Придумай и реализуй 10 различных конструкторов для него. Каждый конструктор должен иметь смысл.
*/

public class Solution
{
    public static void main(String[] args)
    {

    }

    public static class Human
    {
        private String name;//напишите тут ваши переменные и конструкторы
        private String surname;//напишите тут ваши переменные и конструкторы
        private String midsurname;
        private int age;//напишите тут ваши переменные и конструкторы
        private int height;//напишите тут ваши переменные и конструкторы
        private int weight;

        public Human(String name, String surname)
        {
            this.name = name;
            this.surname = surname;
        }

        public Human(String name, String surname, String midsurname)
        {
            this.name = name;
            this.surname = surname;
            this.midsurname = midsurname;
        }

        public Human(String name, String midsurname, int age)
        {
            this.name = name;
            this.midsurname = midsurname;
            this.age = age;
        }

        public Human(String name, String surname, String midsurname, int age)
        {
            this.name = name;
            this.surname = surname;
            this.midsurname = midsurname;
            this.age = age;
        }

        public Human(String name, String surname, int age, int height)
        {
            this.name = name;
            this.surname = surname;
            this.age = age;
            this.height = height;
        }

        public Human(String name, String surname, String midsurname, int age, int height)
        {
            this.name = name;
            this.surname = surname;
            this.midsurname = midsurname;
            this.age = age;
            this.height = height;
        }

        public Human(String name, String surname, int age, int height, int weight)
        {
            this.name = name;
            this.surname = surname;
            this.age = age;
            this.height = height;
            this.weight = weight;
        }

        public Human(String name, String surname, String midsurname, int age, int height, int weight)
        {
            this.name = name;
            this.surname = surname;
            this.midsurname = midsurname;
            this.age = age;
            this.height = height;
            this.weight = weight;
        }

        public Human(String name)
        {
            this.name = name;
        }

        public Human(int age, String name) {
            this.age = age;
            this.name = name;
        }



        //напишите тут ваши переменные и конструкторы
    }
}
