package com.javarush.test.level13.lesson11.home03;

/* Чтение файла
1. Считать с консоли имя файла.
2. Вывести в консоль(на экран) содержимое файла.
3. Не забыть освободить ресурсы. Закрыть поток чтения с файла и поток ввода с клавиатуры.
*/

import java.io.*;
import java.util.Scanner;

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        Scanner sc = new Scanner(System.in);
        String filename = sc.nextLine();

        InputStream text = new FileInputStream(filename);     //  читаем файл
        while (text.available() > 0)                    //  читаем пока не закончатся символы
        {
            System.out.print((char) text.read());       // выводим прочитанный символ
        }
        //--------------------------------
        sc.close();                                 // закрываем поток чтения с клавиатуры
        text.close();



         /*   InputStream file = new FileInputStream("C:\\rush1\\" + filename + ".txt");
            //System.out.println("available: " + file.available());
            while (file.available() > 0)
            {
                System.out.print((char) file.read());
            }

        file.close();*/

    }
}
