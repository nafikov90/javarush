package com.javarush.test.level14.lesson08.bonus01;

import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

/* Нашествие эксепшенов
Заполни массив exceptions 10 различными эксепшенами.
Первое исключение уже реализовано в методе initExceptions.
*/

public class Solution
{
    public static List<Exception> exceptions = new ArrayList<Exception>();

    public static void main(String[] args)
    {
        initExceptions();

        for (Exception exception : exceptions)
        {
            System.out.println(exception);
        }
    }

    private static void initExceptions()
    {   //it's first exception
        try
        {
            float i = 1 / 0;

        } catch (Exception e)
        {
            exceptions.add(e);
        }

        try
        {
            int[] arr = {1, 0};
            arr[2] = 1;

        } catch (Exception e)
        {
            exceptions.add(e);
        }

        try
        {
            Object[] s = new Integer[4];
            s[0] = 4.4;
        } catch (Exception e)
        {
            exceptions.add(e);
        }

        try
        {
            Object ch = new Character('*');
            System.out.println((Byte)ch);

        } catch (Exception e)
        {
            exceptions.add(e);
        }

        try
        {
            int[] nNegArray = new int[-5];

        } catch (Exception e)
        {
            exceptions.add(e);
        }

        try
        {
            int[] nullArr = new int[5];
            nullArr = null;
            int a = nullArr.length;

        } catch (Exception e)
        {
            exceptions.add(e);
        }

        try
        {
            String s = "spartak";
            char c = s.charAt(100);

        } catch (Exception e)
        {
            exceptions.add(e);
        }

        try
        {
            File file = new File("D://file.txt");
            FileReader fr = new FileReader(file);

        } catch (Exception e)
        {
            exceptions.add(e);
        }

        try
        {
            String s = "sd";
            int a = Integer.parseInt(s);

        } catch (Exception e)
        {
            exceptions.add(e);
        }

        Exception ss = new IllegalArgumentException();
        exceptions.add(ss);//Add your code here

    }
}
