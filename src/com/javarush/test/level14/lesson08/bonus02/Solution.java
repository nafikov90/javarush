package com.javarush.test.level14.lesson08.bonus02;

/* НОД
Наибольший общий делитель (НОД).
Ввести с клавиатуры 2 целых положительных числа.
Вывести в консоль наибольший общий делитель.
*/

import java.io.File;
import java.util.Scanner;

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        Scanner sc = new Scanner(System.in);
        int a = sc.nextInt();
        int b = sc.nextInt();
        System.out.println(maxDevisor(a, b));
    }

    static int maxDevisor(int a, int b) {
        if (a % b == 0 || b % a == 0) {
            return Math.min(a, b);
        } else
        {
            for (int i = Math.min(a, b) / 2; i > 0; i--)
            {
                if (a % i == 0 && b % i == 0)
                {
                    return i;
                }
            }
        }
        return 1;
    }
}
