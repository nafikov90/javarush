package com.javarush.test.level14.lesson08.home02;

public interface HolidayName
{
    String getHolidayName();
}
