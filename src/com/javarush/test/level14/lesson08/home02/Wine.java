package com.javarush.test.level14.lesson08.home02;

public class Wine extends Drink implements HolidayName
{
    public String getHolidayName() {
        return "День рождения";
    }
}
