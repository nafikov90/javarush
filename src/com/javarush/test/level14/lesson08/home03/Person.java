package com.javarush.test.level14.lesson08.home03;

public interface Person
{
    void doWork();

    public static class User implements Person
    {
        public void doWork() {
            live();
        }

        void live()
        {
            System.out.println("Usually I just live");
        }
    }

    public static class Looser implements Person
    {

        public void doWork() {
            doNothing();
        }
        void doNothing()
        {
            System.out.println("Usually I do nothing");
        }
    }

    public static class Coder implements Person
    {
        public void doWork() {
            coding();
        }
        void coding()
        {
            System.out.println("Usually I create code");
        }
    }

    public static class Proger implements Person
    {
        public void doWork() {
            enjoy();
        }
        void enjoy()
        {
            System.out.println("Wonderful life!");
        }
    }

}
