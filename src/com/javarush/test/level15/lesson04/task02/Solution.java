package com.javarush.test.level15.lesson04.task02;

/* ООП - Перегрузка
Перегрузите метод printMatrix 8 различными способами. В итоге должно получиться 10 различных методов printMatrix.
*/

import java.awt.*;

public class Solution {
    public static void main(String[] args) {
        printMatrix(2, 3, "8");
        printMatrix(true);
        printMatrix(true, false);
        printMatrix("aaaa");
        printMatrix("A" + 5);
        printMatrix("G", false);
        printMatrix("A", "B");
        printMatrix("OO", 333333333);
        printMatrix("A", 1, 2);
        printMatrix(1, 2.5);

    }

    public static void printMatrix(int m, int n, String value) {
        System.out.println("Заполняем объектами String");
        printMatrix(m, n, (Object) value);
    }

    public static void printMatrix(int m, int n, Object value) {
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(value);
            }
            System.out.println();
        }
    }

    public static void printMatrix(boolean b) {
        System.out.println(b);

    }

    public static void printMatrix(boolean b, boolean a) {
        System.out.println(b);
        System.out.println(a);

    }
    public static void printMatrix(String b) {
        System.out.println(b);

    }

    public static void printMatrix(String b, int a) {
        System.out.println(b + a);

    }

    public static void printMatrix(String b, boolean a) {
        System.out.println(b);
        System.out.println(a);

    }

    public static void printMatrix(String b, String a) {
        System.out.println(b + a);

    }

    public static void printMatrix(String b, long x) {
        System.out.println(b + x);
    }

    public static void printMatrix(String b, int x, int y) {
        System.out.println(b);
    }

    public static void printMatrix(Color color) {
        System.out.println("");
    }

    public static void printMatrix(int x, double d) {
        System.out.println("");
    }
}
