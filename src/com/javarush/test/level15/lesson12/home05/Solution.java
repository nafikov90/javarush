package com.javarush.test.level15.lesson12.home05;

/* Перегрузка конструкторов
1. В классе Solution создайте по 3 конструктора для каждого модификатора доступа.
2. В отдельном файле унаследуйте класс SubSolution от класса Solution.
3. Внутри класса SubSolution создайте конструкторы командой Alt+Insert -> Constructors.
4. Исправьте модификаторы доступа конструкторов в SubSolution так, чтобы они соответствовали конструкторам класса Solution.
*/

public class Solution {
    Solution() {}
    Solution(int i) {}
    Solution(String s) {}

    private Solution(char c) {}
    private Solution(int i, char c) {}
    private Solution(String s, char c) {}

    protected Solution(long l) {}
    protected Solution(int i, long l) {}
    protected Solution(String s, long l) {}

    public Solution(byte b) {}
    public Solution(int i, byte b) {}
    public Solution(String s, byte b) {}
    }


