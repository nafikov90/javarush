package com.javarush.test.level15.lesson12.home05;

public class SubSolution extends Solution
{
    SubSolution()
    {
    }

    SubSolution(int i)
    {
        super(i);
    }

    SubSolution(String s)
    {
        super(s);
    }

    protected SubSolution(long l)
    {
        super(l);
    }

    protected SubSolution(int i, long l)
    {
        super(i, l);
    }

    protected SubSolution(String s, long l)
    {
        super(s, l);
    }

    public SubSolution(byte b)
    {
        super(b);
    }

    public SubSolution(int i, byte b)
    {
        super(i, b);
    }

    public SubSolution(String s, byte b)
    {
        super(s, b);
    }

    private SubSolution(char c) {
        super(c);
    }
    private SubSolution(int i, char c) {
        super(i, c);
    }
    private SubSolution(String s, char c) {
        super(s, c);
    }
}
