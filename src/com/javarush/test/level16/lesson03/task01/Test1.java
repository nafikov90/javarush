package com.javarush.test.level16.lesson03.task01;

public class Test1 extends Thread
{
    public static void main(String[] args)
    {
        System.out.println("main to go");
        Sat ss = new Sat();
        Sat zz = new Sat();

        System.out.println("main over");
    }

    public static class Sat implements Runnable {
        Thread z;

        Sat() {
            z = new Thread(this, "Ufa");
            System.out.println("ufa created");
            z.start();
            try {
                z.join();
            } catch (InterruptedException e) {}
        }

        public void run() {
            synchronized (z)
            {
                System.out.println("run Ufa");
                System.out.println(Thread.currentThread());

                try
                {
                    for (int i = 5; i > 0; i--)
                    {
                        System.out.println(i);
                        Thread.sleep(500);
                    }
                }
                catch (InterruptedException e)
                {
                }
                System.out.println("ufa the end");
            }
        }
    }
}
