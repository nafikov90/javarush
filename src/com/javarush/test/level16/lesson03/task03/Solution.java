package com.javarush.test.level16.lesson03.task03;

import java.util.ArrayList;
import java.util.List;

/* Список и нити
В методе main добавить в статический объект list 5 нитей SpecialThread - различных объектов.
*/

public class Solution {
    public static volatile List<Thread> list = new ArrayList<Thread>(5);

    public static void main(String[] args) {
        SpecialThread st1 = new SpecialThread();
        Thread t1 = new Thread(st1);
        list.add(t1);
        //t1.start();//Add your code here - добавьте свой код тут
        SpecialThread st2 = new SpecialThread();
        Thread t2 = new Thread(st2);
        list.add(t2);
        //t2.start();//Add your code here - добавьте свой код тут
        SpecialThread st3 = new SpecialThread();//Add your code here - добавьте свой код тут
        Thread t3 = new Thread(st3);
        list.add(t3);
        //t3.start();
        SpecialThread st4 = new SpecialThread();//Add your code here - добавьте свой код тут
        Thread t4 = new Thread(st4);
        list.add(t4);
        //t4.start();
        SpecialThread st5 = new SpecialThread();//Add your code here - добавьте свой код тут
        Thread t5 = new Thread(st5);
        list.add(t5);
        //t5.start();
    }

    public static class SpecialThread implements Runnable {
        public void run() {
            System.out.println("it's run method inside SpecialThread");
        }
    }
}
