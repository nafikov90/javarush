package com.javarush.test.level16.lesson05.task01;

public class DemoJoin
{
    public static void main(String[] args)
    {
        Test12 ob1 = new Test12("One");
        Test12 ob2 = new Test12("Two");
        Test12 ob3 = new Test12("Three");

        System.out.println("stream 1 start " + ob1.t.isAlive());
        System.out.println("stream 2 start " + ob2.t.isAlive());
        System.out.println("stream 3 start " + ob3.t.isAlive());


        try {
            System.out.println("Wait the end of threads");
            ob1.t.join();
            ob1.t.wait(3000);
            ob2.t.join();
            ob3.t.join();
        } catch (InterruptedException e) {
            System.out.println("main thread break");
        }

        System.out.println("stream 1 start " + ob1.t.isAlive());
        System.out.println("stream 2 start " + ob2.t.isAlive());
        System.out.println("stream 3 start " + ob3.t.isAlive());

        System.out.println("main the end");
    }
}
