package com.javarush.test.level16.lesson05.task01;

public class Test12 implements Runnable
{
    String name;
    Thread t;

    Test12(String name) {
        this.name = name;
        t = new Thread(this, name);
        System.out.println("New thread: " + t);
        t.start();
    }

    public void run() {
        try {
            for (int i = 5; i > 0; i--)
            {
                System.out.println(name + " " + i);
                Thread.sleep(5000);
            }
        } catch (InterruptedException a) {
            System.out.println(name + " prervan" );
        }
    }
}