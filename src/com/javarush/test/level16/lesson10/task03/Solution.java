package com.javarush.test.level16.lesson10.task03;

/* Снова interrupt
Создай нить TestThread.
В методе main создай экземпляр нити, запусти, а потом прерви ее используя метод interrupt().
*/

import static java.lang.Thread.sleep;

public class Solution {
    public static void main(String[] args) throws InterruptedException {
        TestThread t1 = new TestThread();
        t1.start();
        sleep(1000);
        t1.interrupt();//Add your code here - добавь код тут
    }

    //Add your code below - добавь код ниже
    public static class TestThread  extends Thread {
        public void run() {

        }
    }
}
